
# coding-exam

## Authentication

The Coding Exam API uses [Bearer Tokens](https://learning.postman.com/docs/sending-requests/authorization/#bearer-token) to authenticate requests. Your API token is provided to you at the start of the exam. 

## Response

| Attribute | Description |
| --- | --- |
| result | Indicates if API request is success or not. Values are `ok` if success, `error` if not. |
| data | The `data` attribute only shows if the API request is a success. It contains the data from the request. | 
| error | The `error` attribute only shows if the API request returns an error. It contains the attributes: `name` (the error name) and `message` (the error description) |

## Errors

| Name | Description |
| --- | ---|
| AuthenticationError | Invalid authentication token |
| NotFoundError | Endpoint is non-existent |
| ValidationError | MongoDB validation error |

## The Employee object

### Attributes
| Name | Description | Possible Values
| --- | --- | --- | 
| first_name | First Name  
| last_name | Last name 
| birthdate | Birthdate 
| address | Address
| status | Status | `probationary`, `regular`, `resigned`
| team | Team | `it`, `hr`, `finance`

### Methods 

#### Creating an employee

* Creates an employee. 

##### URL

* POST https://api.exams.kloud.ai/employees

##### Parameters

| Name | Required | 
| --- | --- |
| first_name | YES
| last_name | YES
| birthdate | YES 
| address | NO
| status | YES 
| team | YES 

##### Returns

The created Employee object is returned if successful. Otherwise, this call returns an error.

#### Retrieve an employee

##### URL

* GET https://api.exams.kloud.ai/employees/:identifier

##### Parameters

No parameters needed.

##### Returns

Returns an Employee if a valid identifier was provided.

#### Retrieve employees

##### URL

* GET https://api.exams.kloud.ai/employees/

##### Parameters

No parameters needed.

##### Returns

Returns created Employees.

#### Update an employee

##### URL

* PUT https://api.exams.kloud.ai/employees/:identifier

##### Parameters

| Name | Required | 
| --- | --- |
| first_name | NO
| last_name | NO
| birthdate | NO 
| address | NO
| status | NO 
| team | NO 

##### Returns

The updated Employee object.


#### Delete an employee

##### URL

* POST https://api.exams.kloud.ai/employees/:identifier/delete

##### Parameters

No parameters needed.

##### Returns

A confirmation response. The deleted Employee object won't be returned.
